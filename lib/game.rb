require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new

    @player_one.mark = :O
    @player_two.mark = :X

    @current_player = @player_one
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play
    until board.over?
      play_turn
    end
    conclude
  end


  def play_turn
    current_player.display(@board)
    move = current_player.get_move
    @board.place_mark(move, current_player.mark)
    switch_players!
  end

  def conclude
    puts @board.display
    puts "#{@board.winner} has won."
  end
end


if __FILE__ == $PROGRAM_NAME
  p1 = HumanPlayer.new("Andrew")
  p2 = ComputerPlayer.new("Hal")
  g = Game.new(p1, p2)
  g.play
end
